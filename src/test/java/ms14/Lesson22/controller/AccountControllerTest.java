package ms14.Lesson22.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ms14.Lesson22.dto.AccountRequestDto;
import ms14.Lesson22.dto.AccountResponseDto;
import ms14.Lesson22.model.Account;
import ms14.Lesson22.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.JsonPath;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AccountController.class)
@RunWith(SpringRunner.class)
class AccountControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;
    @MockBean
    private AccountService accountService;


    @Test
    void givenValidIdWhenGetAccountThenSuccess() throws Exception {
        Account account = Account.builder()
                .id(600L)
                .name("Humay")
                .balance(111.1)
                .build();

        when(accountService.getById(anyLong())).thenReturn(account);
        mockMvc.perform(get("/account/600")
                        .accept(APPLICATION_JSON))
                .andExpect(status()
                        .isOk())
                .andExpect(header()
                        .exists("hello"))
                .andExpect(content()
                        .json(objAsJson(account)))
                .andExpect(jsonPath("$.id")
                        .value("600"))
                .andExpect(jsonPath("$.name")
                        .value("Humay"))
                .andExpect(jsonPath("$.balance")
                        .value("111.1"));
    }


    @Test
    void givenValidAccountRequestDtoWhenCreatedThenSuccess() throws Exception {
        AccountRequestDto accountRequestDto = AccountRequestDto
                .builder()
                .name("Humay")
                .balance(100.0)
                .build();

        when(accountService.create(any())).thenReturn(AccountResponseDto.builder()
                .name("Humay")
                .balance(100.0)
                .id(100L)
                .build());

        mockMvc.perform(post("/account")
                        .contentType(APPLICATION_JSON)
                        .content(objAsJson(accountRequestDto))
                        .accept(APPLICATION_JSON))
                .andExpect(status()
                        .isCreated())
                .andExpect(jsonPath("$.name")
                        .value("Humay"))
                .andExpect(jsonPath("$.id")
                        .value(100L))
                .andExpect(jsonPath("$.balance")
                        .value(100.0));
    }

    private String objAsJson(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }

}