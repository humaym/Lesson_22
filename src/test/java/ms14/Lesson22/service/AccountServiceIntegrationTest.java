package ms14.Lesson22.service;

import ms14.Lesson22.dto.AccountRequestDto;
import ms14.Lesson22.model.Account;
import ms14.Lesson22.repository.AccountRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class AccountServiceIntegrationTest {

    @Autowired
    AccountService accountService;
    @Autowired
    AccountRepository accountRepository;


    @Container
    public static MySQLContainer mysql = new MySQLContainer("mysql:latest")
            .withDatabaseName("ms14-test")
            .withUsername("root")
            .withPassword("password");

    @Container
    public static GenericContainer redis=new GenericContainer("redis:latest");


    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mysql::getJdbcUrl);
        registry.add("spring.datasource.username", mysql::getUsername);
        registry.add("spring.datasource.password", mysql::getPassword);
        registry.add("spring.datasource.driver-class-name", mysql::getDriverClassName);
        registry.add("spring.jpa.hibernate.ddl-auto", ()-> "update");
    }

    @BeforeAll
    public static void setUp() {
        mysql.start();
    }

    @Test
    public void testGetAccountById() {
        //Arrange
        Account account = accountRepository.save(Account.builder()
                .name("Humay")
                .balance(11111.1)
                .build());

//        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        //Act
        Account byId = accountService.getById(account.getId());
        //Assert
        assertThat(byId).isEqualTo(account);
        assertThat(byId.getBalance()).isEqualTo(11111.1);
    }

    @Test
    public void testCreateAccount() {
        //Arrange
        AccountRequestDto accountRequestDto = AccountRequestDto.builder()
                .name("Humay")
                .balance(123.12)
                .build();

        //Act
        var byId = accountService.create(accountRequestDto);

        //Assert
        assertThat(byId.getId()).isNotNull();
        assertThat(byId.getName()).isEqualTo("Humay");
        assertThat(byId.getBalance()).isEqualTo(123.12);

        List<Account> all = accountRepository.findAll();
        assertThat(all.size()).isEqualTo(1);
        assertThat(all.get(0).getName()).isEqualTo("Humay");
        assertThat(all.get(0).getBalance()).isEqualTo(123.12);
        assertThat(all.get(0).getId()).isEqualTo(byId.getId());
    }


    @AfterAll
    public static void tearDown() {
        mysql.stop();
    }
}