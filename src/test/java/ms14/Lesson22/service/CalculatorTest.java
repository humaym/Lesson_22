package ms14.Lesson22.service;

import org.assertj.core.api.AbstractBigDecimalAssert;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class CalculatorTest {

    @InjectMocks
    private Calculator calculator;

    @Test
    void givenTwoDigitsWhenSumThenSuccess() {
        //Arrange
        int a = 5;
        int b = 10;

        //Act
        int sum = calculator.sum(a, b);

        //Assert
        assertThat(sum).isEqualTo(a + b+1);
    }

    @Test
    void givenTwoDigitsWhenDivideThenSuccess() {
        int a = 10;
        int b = 5;
        int divide = calculator.divide(a, b);
        assertThat(divide).isEqualTo(a / b);
    }

    @Test
    void givenTwoDigitsWhenDivideToZeroThenException()  {
        int a = 10;
        int b = 0;
        assertThatThrownBy(() -> calculator.divide(a, b))
                .isInstanceOf(Throwable.class)
                .hasMessage("Divide to zero is prohibited");
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 7, 9, -25})
    void givenNumberWhenOddReturnTrue(int a) {
        var result = calculator.isOdd(a);
        assertThat(result).isTrue();
    }
}