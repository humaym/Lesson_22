package ms14.Lesson22.service;

import ms14.Lesson22.dto.AccountRequestDto;
import ms14.Lesson22.dto.AccountResponseDto;
import ms14.Lesson22.model.Account;
import ms14.Lesson22.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private ModelMapper modelMapper;
    private Account mockAccount;
    private AccountResponseDto accountResponseDto;
    private AccountRequestDto accountRequestDto;

    @BeforeEach
    void setUp() {
        mockAccount = Account.builder()
                .id(600L)
                .name("Humay")
                .balance(111.1)
                .build();

        accountRequestDto = AccountRequestDto.builder()
                .name("Humay")
                .balance(111.1)
                .build();

        accountResponseDto = AccountResponseDto.builder()
                .name("Humay")
                .id(600L)
                .balance(111.1)
                .build();
    }


    @Test
    void givenValidIdThenGetAccountThenSuccess() {
        long id = 600L;
        when(accountRepository.findById(id)).thenReturn(Optional.of(mockAccount));
        Account account = accountService.getById(id);
        assertThat(account.getId()).isEqualTo(id);
        assertThat(account.getName()).isEqualTo(mockAccount.getName());
        assertThat(account.getBalance()).isEqualTo(mockAccount.getBalance());
        verify(accountRepository, times(1)).findById(id);
    }

    @Test
    void givenValidIdThenGetAccountThenNotFound() {
        long id = 6;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> accountService.getById(id))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void givenValidDtoWhenCreateAccountThenSuccess() {
        //Arrange
        Long id = 600L;

        when(modelMapper.map(any(AccountRequestDto.class), eq(Account.class)))
                .thenReturn(mockAccount);
        when(accountRepository.save(any())).thenReturn(mockAccount);
        when(modelMapper.map(any(Account.class), eq(AccountResponseDto.class)))
                .thenReturn(accountResponseDto);


        //Act
        AccountResponseDto response = accountService.create(accountRequestDto);

        //Assert
        assertThat(response.getName()).isEqualTo(accountRequestDto.getName());
        assertThat(response.getBalance()).isEqualTo(accountRequestDto.getBalance());
        assertThat(response.getId()).isEqualTo(mockAccount.getId());

        verify(accountRepository, times(1)).save(any());

        var captor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository, times(1)).save(captor.capture());

        assertThat(captor.getValue().getId()).isEqualTo(600L);
        assertThat(captor.getValue().getName()).isEqualTo("Humay");
        assertThat(captor.getValue().getBalance()).isEqualTo(111.1);
    }

    @Test
    void givenValidIdWhenUpdateThenSuccess() {
        //Arrange
        Long id = 600L;
        when(accountRepository.findById(id)).thenReturn(Optional.of(mockAccount));
        Account updatedAccount = Account.builder()
                .name("Humay")
                .balance(111.1)
                .build();
        when(accountRepository.save(any())).thenReturn(updatedAccount);
        when(modelMapper.map(any(Account.class), eq(AccountResponseDto.class)))
                .thenReturn(AccountResponseDto
                        .builder()
                        .name("Humay")
                        .balance(111.1)
                        .build());

        //Act
        AccountResponseDto response = accountService.update(id, accountRequestDto);
        //Assert
        assertThat(response.getName()).isEqualTo(mockAccount.getName());
        assertThat(response.getBalance()).isEqualTo(mockAccount.getBalance());
    }

    @Test
    void getAllAccountsThenSuccess() {
        List<Account> mockAccounts = new ArrayList<>();
        mockAccounts.add(new Account(100L, "Humay", 100.0));
        mockAccounts.add(new Account(200L, "Humay2", 1000.0));
        when(accountRepository.findAll())
                .thenReturn(mockAccounts);
        when(modelMapper.map(mockAccounts.get(0), AccountResponseDto.class))
                .thenReturn(new AccountResponseDto(100L, "Humay", 100.0));
        when(modelMapper.map(mockAccounts.get(1), AccountResponseDto.class))
                .thenReturn(new AccountResponseDto(200L, "Humay2", 1000.0));
        List<AccountResponseDto> responseDtos = accountService.getAll();
        assertThat(responseDtos.size()).isEqualTo(mockAccounts.size());
        assertEquals(2, responseDtos.size());
        assertEquals(100L, responseDtos.get(0).getId());
        assertEquals("Humay", responseDtos.get(0).getName());
        assertEquals(1000.0, responseDtos.get(1).getBalance());


    }

    @Test
    void givenValidIdWhenDeleteThenSuccess() {
        Long id = 600L;
        when(accountRepository.findById(id)).thenReturn(Optional.of(mockAccount));
        when(modelMapper.map(any(Account.class), eq(AccountResponseDto.class))).thenReturn(accountResponseDto);
        AccountResponseDto responseDto = accountService.delete(mockAccount.getId());
        assertThat(responseDto.getBalance()).isEqualTo(mockAccount.getBalance());
        assertThat(responseDto.getId()).isEqualTo(mockAccount.getId());
        assertThat(responseDto.getName()).isEqualTo(mockAccount.getName());
    }

    @Test
    void givenValidIdWhenNotFoundThenReturnException() {
        long id = 600L;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> accountService.delete(id))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Account with id 600 not found");
    }

    @Test
    void givenValidIdWhenNotFoundThenException() {
        long id = 600L;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> accountService.update(id, accountRequestDto))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Account not found");
    }
}