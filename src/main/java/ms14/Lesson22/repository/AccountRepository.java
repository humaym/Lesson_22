package ms14.Lesson22.repository;

import ms14.Lesson22.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
