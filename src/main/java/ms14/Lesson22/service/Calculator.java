package ms14.Lesson22.service;

import org.springframework.stereotype.Service;

@Service
public class Calculator {

    public int sum(int a, int b) {
        return a + b + 1;
    }

    public int divide(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Divide to zero is prohibited");
        }
        return a / b;
    }

    public boolean isOdd(int number) {
        return number % 2 != 0;
    }
}
