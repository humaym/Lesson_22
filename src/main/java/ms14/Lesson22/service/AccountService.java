package ms14.Lesson22.service;

import lombok.RequiredArgsConstructor;
import ms14.Lesson22.dto.AccountRequestDto;
import ms14.Lesson22.dto.AccountResponseDto;
import ms14.Lesson22.model.Account;
import ms14.Lesson22.repository.AccountRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final ModelMapper modelMapper;

    public Account getById(Long id) {
//        Account account = accountRepository.findById(id).get();
        return accountRepository.findById(id).orElseThrow(() -> new RuntimeException("Account not found"));
    }

    public AccountResponseDto create(AccountRequestDto dto) {
        Account account = modelMapper.map(dto, Account.class);
        account = accountRepository.save(account);
        return modelMapper.map(account, AccountResponseDto.class);
    }

    public AccountResponseDto update(Long id, AccountRequestDto accountRequestDto) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new RuntimeException("Account not found"));
        account.setBalance(accountRequestDto.getBalance());
        account.setName(accountRequestDto.getName());
        accountRepository.save(account);
        return modelMapper.map(account, AccountResponseDto.class);
    }

    public List<AccountResponseDto> getAll() {
        return accountRepository.findAll().stream()
                .map(account -> modelMapper
                        .map(account, AccountResponseDto.class))
                .collect(Collectors.toList());
    }

    public AccountResponseDto delete(Long id){
        Account account=accountRepository.findById(id).orElseThrow(()->new RuntimeException(String.format("Account with id %s not found",id)));
        accountRepository.delete(account);
        return modelMapper.map(account,AccountResponseDto.class);
    }
}
