package ms14.Lesson22.controller;

import lombok.RequiredArgsConstructor;
import ms14.Lesson22.dto.AccountRequestDto;
import ms14.Lesson22.dto.AccountResponseDto;
import ms14.Lesson22.model.Account;
import ms14.Lesson22.repository.AccountRepository;
import ms14.Lesson22.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/account")
@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final AccountRepository accountRepository;

    @GetMapping("/{id}")
    public ResponseEntity<Account> getById(@PathVariable Long id) {
        Account account =accountService.getById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .header("hello","12345")
                .body(account);
    }

    @PostMapping
    public ResponseEntity<AccountResponseDto> create(@RequestBody AccountRequestDto dto) {
        return  ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Humay2","1222")
                .body(accountService.create(dto));
    }

    @PutMapping("/{id}")
    public AccountResponseDto update(@PathVariable Long id, @RequestBody AccountRequestDto accountRequestDto) {
        return accountService.update(id, accountRequestDto);
    }

    @GetMapping
    public List<AccountResponseDto> getAll() {
        return accountService.getAll();
    }
}
